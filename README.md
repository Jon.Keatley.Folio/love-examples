## Info

All examples have been created using the current version of LÖVE (11.1). They have been tested on Windows 10 and Linux (Mint)

## Examples

### Chop Chop Timber

[Link](chopchop)

Chop Chop Timber is a tiny game I made when I was learning Lua. I ported over some of the main game elements I have used to make games with other platforms.

![Chop Chop Timber Screenshot](images/chopchop.png)

### Voronoi

[Link](voronoi)

Voronoi is an initial attempt to dynamically create a Voronoi diagram using a pixel shader. Add points by clicking.  
https://en.wikipedia.org/wiki/Voronoi_diagram

![Voronoi Screenshot](images/voronoi.png)

### Retro

Retro is a work in progress. I am attempting to use pixel shaders to both dynamically down-grade art assets and simulate a cathode ray tube (CRT). My next step is implement colour bleed.

[Link](retro)

![Retro Screenshot](images/retro.png)
