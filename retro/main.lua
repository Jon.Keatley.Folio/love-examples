
local floor = math.floor
local rect = love.graphics.rectangle


lowres = love.graphics.newShader [[
    const vec4 light_color = vec4(0.988,1,0.027,0.8); //252 255 7
    const vec2 light = vec2(200,256);
    const float size = 800;
    
    uniform float res;
  
	vec4 effect(vec4 global_color, Image texture, vec2 texture_coords, vec2 pixel_coords)
    {
		vec2 scale_coords = vec2(texture_coords.x,texture_coords.y);
		
		float fres = res;
		
		if(scale_coords.x > 0)
		{
			scale_coords.x = floor(scale_coords.x / fres) * fres;
		}
		
		if(scale_coords.y > 0)
		{
			scale_coords.y = floor(scale_coords.y / fres) * fres;
		}
    
        vec4 pixel = Texel(texture, scale_coords);
        
        return pixel;  
    }
]]

tv = love.graphics.newShader [[
  
	const vec4 blank = vec4(0,0,0,0);
	const vec4 line = vec4(0.6, 1, 0.4,0.2);
	const float distance = 60;
		
	vec4 effect(vec4 global_color, Image texture, vec2 texture_coords, vec2 pixel_coords)
    {
		vec4 result = blank;
		if(mod(floor(pixel_coords.y),2) == 0.0)
		{
			result = line;
		}
		
		float action = -1;
		
		if(pixel_coords.x <= distance && pixel_coords.y <= distance) //top left 
		{
			action = (pixel_coords.x + pixel_coords.y);
		}
		else if(pixel_coords.x <= distance && pixel_coords.y >= love_ScreenSize.y - distance) //bottom left
		{
			action = pixel_coords.x + (love_ScreenSize.y - pixel_coords.y);
		}
		else if(pixel_coords.x >= love_ScreenSize.x - distance && pixel_coords.y <= distance) //top right
		{
			action = (love_ScreenSize.x -pixel_coords.x) + pixel_coords.y;
		}
		else if(pixel_coords.x >= love_ScreenSize.x - distance && pixel_coords.y >= love_ScreenSize.y - distance) //bottom right
		{
			action = (love_ScreenSize.x -pixel_coords.x) + (love_ScreenSize.y - pixel_coords.y);
		}

		if(action > -1)
		{
			float mod =  1 - (action / (distance ));
			if(mod > 0)
			{
				result.x -= (result.x * mod);
				result.y -= (result.y * mod);
				result.z -= (result.z * mod);
				result.w -= (result.w * mod);
			}
		}
		
        return result;  
    }
]]


local screensize = love.graphics.getWidth()

local x,y
local xv = 0.0001;
local yv = -1;


x = 1;
y = 0;

function love.load()

	logo = love.graphics.newImage("olli.jpg");
	scale = love.graphics.newQuad(0,0,800,800,800,800);
	
	lowres:send("res",x);
end

function love.draw()

  love.graphics.setShader(lowres)
  love.graphics.draw(logo,scale,62,62,0,0.5,0.5);


  y = y + 1;
  if y > 100 then
	y = 0;
	  lowres:send("res",x);
	  
	  x = x + xv;
	  
	  if x >= 0.05 then
	   x = xv;
	  end
	end
	
	love.graphics.setShader(tv)
	love.graphics.rectangle("fill", 0, 0, 512, 512 )
	
end
