local floor = math.floor
local rect = love.graphics.rectangle

local points = {}
local colors = {}

local physics = {}
local objects = {}
local blocks = {}
local tower = {}
local editor = {}
local controllers = {}
local sounds = {}
local current_level = 1

-- utils -----------------------------------------------------------------------
function prequire(package)
  local ok, err = pcall(require, package)
  if not ok then return nil, err end
  return err
end

-- love events -----------------------------------------------------------------
function love.load()

  -- block settings
  print("Screen is " .. love.graphics.getWidth() .. " x " .. love.graphics.getHeight())

  blocks.width = love.graphics.getWidth() * 0.025
  blocks.height = love.graphics.getHeight() * 0.05
  blocks.half_width = blocks.width / 2
  blocks.types = {"square", "block", "slab", "long_slab", "triangle"}
  print("Block size " .. blocks.width .. " x " .. blocks.height)

  -- tower settings
  tower.width = love.graphics.getWidth() / blocks.width
  tower.height = love.graphics.getHeight() / blocks.height
  tower.next_level = tower.height - 1
  tower.floor_mode = false
  tower.goal = love.graphics.getHeight() * 0.70
  tower.target_bottom = love.graphics.getHeight() * 0.90
  tower.target_top = love.graphics.getHeight() * 0.30
  tower.target_vel = blocks.height * 6
  tower.target = tower.target_bottom
  print("Tower grid " .. tower.width .. " x " .. tower.height)

  -- levels settings
  levels_mod = prequire("levels")
  if levels_mod then
    print(#levels .. " levels found")
  else
    print("no levels found")
    levels = {}
  end

  -- editor settings
  editor.edit_mode = #levels == 0
  editor.shape = blocks.types[1]
  editor.shape_index = 1
  editor.pointer_x = 0
  editor.pointer_y = 0
  editor.save = {}

  -- box2D settings
  physics.height = love.graphics.getHeight()
  physics.width = love.graphics.getWidth()
  physics.world = love.physics.newWorld(0,60,true)
  physics.ground = love.physics.newBody(physics.world,physics.width / 2,physics.height - (blocks.height / 2), "static")
  physics.groundFixture = love.physics.newFixture(physics.ground, love.physics.newRectangleShape(physics.width,blocks.height), 40)
  physics.gbX, physics.gbY, physics.gbW, physics.gbH = physics.groundFixture:getBoundingBox()
  physics.gbW = physics.gbW - physics.gbX
  physics.gbH = physics.gbH - physics.gbY
  physics.obj_count = 0
  physics.hit_count = 0

  --register joysticks
  local index = 1;
  local sticks = love.joystick.getJoysticks()
  for i, j in ipairs(sticks) do
    print(j:getName() .. " index:" .. index);
    controllers[j] = index;
    index = index + 1;
  end

  --load first level
  if #levels > 0 then
    load_world(levels[current_level])
  end

  --sounds
  sounds.wood = love.sound.newDecoder("resources/wood.ogg")
  sounds.playing = {}
  sounds.soundEvent = function(obja,objb,contact)
    --print(contact:getRestitution())
    if contact:getRestitution() >= 0.5 then
      if obja:getShape():type() == "CircleShape" or objb:getShape():type() == "CircleShape"  then
        physics.hit_count = physics.hit_count + 1
      end
    end
  end

  physics.world:setCallbacks(sounds.soundEvent,nil,nil,nil)

  --shaders
  require("shaders")
end

function love.update(dt)

  physics.world:update(dt,16,6)

  --Update object count
  if physics.obj_count ~= #objects then
    --print("Obj count " .. physics.obj_count)
    physics.obj_count = #objects
  end

  -- cull dead objects
  local min_height = love.graphics.getHeight()
  local kill = -1
  for i=1,#objects do
    if not objects[i].body:isDestroyed() then
      fix = objects[i].fix
      tlx,tly,brx,bry = fix:getBoundingBox(1)
      if tly < min_height then
        min_height = tly
      end
      if brx < 0 or tlx > physics.width then
        --print("culled")
        objects[i].body:destroy()
        if kill == -1 then
          kill = i
        end
      end
    end
  end

  --clear one block from the dead list
  if kill > -1 then
    table.remove(objects,kill)
    --print("Dropped from objects")
  end

  -- check for level end
  if editor.edit_mode~= true and min_height > tower.goal then
    next_level()
  end

  --update target
  tower.target = tower.target + (dt * tower.target_vel)
  if tower.target > tower.target_bottom then
      tower.target_vel = -tower.target_vel
      tower.target = tower.target_bottom
  elseif tower.target < tower.target_top then
      tower.target_vel = -tower.target_vel
      tower.target = tower.target_top
  end


  --play new sounds
  --print("Contacts " .. physics.hit_count)
  --for i =1,physics.hit_count do
  if physics.hit_count > 0 then
    local s = love.audio.newSource(sounds.wood,"stream")
    sounds.playing[#sounds.playing + 1] = s
    s:setVolume(0.5 + love.math.random(0,0.5))
    s:setPitch(0.3 + love.math.random(0,0.4))
    s:play()
  end

  physics.hit_count = 0

  --print("Active sounds " .. #sounds.playing)

  if #sounds.playing > 0 then
    for key,sound in next, sounds.playing do
      if not sound:isPlaying() then
        table.remove(sounds.playing,key)
      end
    end
  end

end

function love.draw()
  -- sky
  love.graphics.clear(0.1,0.6,1,1)

  --ground
  love.graphics.setColor(0,1,0,1)
  love.graphics.rectangle("fill", physics.gbX,physics.gbY,physics.gbW,physics.gbH)

  love.graphics.setShader(block_shader)
  -- objects
  love.graphics.setLineWidth(2)
  love.graphics.setLineStyle("smooth")
  for i=1,#objects do
    o = objects[i]
    if not o.body:isDestroyed() then
      love.graphics.setColor(o.color[1],o.color[2],o.color[3],1)
      if o.is_attack then
        love.graphics.circle("fill",o.body:getX(),o.body:getY(),blocks.half_width)
      else

        love.graphics.polygon("fill",o.body:getWorldPoints(o.fix:getShape():getPoints()))
        love.graphics.setColor(0,0,0,0.2)
        love.graphics.polygon("line",o.body:getWorldPoints(o.fix:getShape():getPoints()))
      end
    end
  end
  love.graphics.setShader()

  love.graphics.setColor(1,1,1,0.5)
  love.graphics.line(0, tower.goal, love.graphics.getWidth(), tower.goal)

  if editor.edit_mode then
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("line", editor.pointer_x, editor.pointer_y, blocks.width, blocks.height)
  end

  love.graphics.setColor(0,0,0,1)
  love.graphics.line(0, tower.target , blocks.width, tower.target)

end

-- block functions -------------------------------------------------------------

function create_block(x,y, type)

  obj = {}
  obj.is_attack = false
  obj.body = love.physics.newBody(physics.world,x,y, "dynamic")

  if type == "square" then
    obj.body:setX(x + (blocks.width / 2))
    obj.fix = love.physics.newFixture(obj.body, love.physics.newRectangleShape(blocks.width,blocks.height), 5)
  elseif type == "block" then
    obj.body:setX(x + (blocks.width / 2))
    obj.fix = love.physics.newFixture(obj.body, love.physics.newRectangleShape(blocks.width,blocks.height * 2), 5)
  elseif type == "slab" then
    obj.body:setX(x + (blocks.width * 1.5))
    obj.fix = love.physics.newFixture(obj.body, love.physics.newRectangleShape(blocks.width * 3,blocks.height), 5)
  elseif type == "long_slab" then
    obj.body:setX(x + (blocks.width * 3))
    obj.fix = love.physics.newFixture(obj.body, love.physics.newRectangleShape(blocks.width * 6,blocks.height), 5)
  elseif type == "triangle" then
    local w1p5 = blocks.width * 1.5
    local hp5  = blocks.height * 0.5
    local points = {-w1p5, hp5, w1p5, hp5, 0, -hp5}
    obj.body:setX(x + (blocks.width * 1.5))
    obj.fix = love.physics.newFixture(obj.body, love.physics.newPolygonShape(points), 10)
  end

  --obj.fix:setRestitution(0.5)
  obj.color = {}
  obj.color[1] = math.random(1,100) / 100
  obj.color[2] = math.random(1,100) / 100
  obj.color[3] = math.random(1,100) / 100
  objects[#objects + 1] = obj
end

function create_attack()
  obj = {}
  obj.is_attack = true
  obj.body = love.physics.newBody(physics.world,0,tower.target, "dynamic")
  obj.body:setLinearVelocity(300,-80)
  obj.fix = love.physics.newFixture(obj.body, love.physics.newCircleShape(blocks.half_width), 10)
  obj.fix:setRestitution(0.5)
  obj.color = {}
  obj.color[1] = 1
  obj.color[2] = 0
  obj.color[3] = 1
  objects[#objects + 1] = obj

end

-- editor methods --------------------------------------------------------------
function select_block_type(change)
  --switch between the various block options
  editor.shape_index = editor.shape_index + change

  if editor.shape_index > #blocks.types then
    editor.shape_index = 1
  elseif editor.shape_index < 1 then
      editor.shape_index = #blocks.types
  end

  editor.shape = blocks.types[editor.shape_index]
  print("Selected type: " .. blocks.types[editor.shape_index])

end

function love.mousereleased(x,y,button,isTouch, presses)
  -- Place a block in the world
  if editor.edit_mode then
  	if button == 1 then
      --create_block(x,y,blocks.types[math.random(#blocks.types)])
      --build_layer()
      local grid_x = math.floor(x / blocks.width)
      local grid_y = math.floor(y / blocks.height)

      create_block(grid_x * blocks.width,grid_y * blocks.height,editor.shape)
      editor.save[#editor.save + 1] = {x=grid_x, y=grid_y, type=editor.shape}
      --blocks.types[math.random(#blocks.types)]
    end
  end
end

function love.mousemoved(x,y,button,isTouch)
  -- update visual aid for where block will be placed
  if editor.edit_mode then
    editor.pointer_x = math.floor(x / blocks.width) * blocks.width
    editor.pointer_y = math.floor(y / blocks.height) * blocks.height
  end

end

function love.keyreleased(key, scancode)
  -- handle keyboard events such as save
  if key == "e" then
    if physics.obj_count > 0 then
     editor.edit_mode = not editor.edit_mode
    else
     print("Unable to end editor mode until a level is built")
    end
  end

  if editor.edit_mode then
    if key == "s" then
      save_world()
    elseif key == "c" then
      clear_world()
    elseif key == "]" then
      select_block_type(1)
    elseif key == "[" then
      select_block_type(-1)
    end
  end

  if key == "escape" then
    love.event.quit()
  elseif key == "r" then
    load_world(levels[current_level])
  elseif key == "n" then
    next_level()
  end
end

-- level io --------------------------------------------------------------------
function save_world()
  print("Saving design")
  local savefile = "levels.lua"
  info = love.filesystem.getInfo(savefile)
  local add_level_def = false
  if not info then
    add_level_def = true
  end

  file = love.filesystem.newFile(savefile)
  if add_level_def then
    file:open("w")
    file:write("levels = {}\n\n")
  else
    file:open("a")
  end

  file:write("-- Level --------------------\n")
  file:write("levels[#levels+1] = {}\n")
  for i=1,#editor.save do
    file:write("levels[#levels][" .. i .. "]={")
    file:write("x= " .. editor.save[i].x .. ",")
    file:write("y= " ..editor.save[i].y .. ",")
    file:write("type= \"" .. editor.save[i].type .. "\"}\n")
  end
  file:write("\n\n")
  file:close()
end

function clear_world()
  print("Clearing world")
  for i=1,#objects do
    if not objects[i].body:isDestroyed() then
        objects[i].body:destroy()
    end
  end

  editor.save = {}
  objects = {}
end

function load_world(level)
  clear_world()
  print("Loading world")
  print(#level)

  for i=1,#level do
    create_block(level[i].x * blocks.width,level[i].y * blocks.height, level[i].type)
    --print(editor.save[i].x, editor.save[i].y, editor.save[i].type)
  end
  physics.last_contact_count = physics.world:getContactCount()
end

function next_level()
  current_level = current_level + 1
  if current_level > #levels then
    current_level = 1
  end
  load_world(levels[current_level])
end

-- inputs --------------------------------------------------------------------
function love.joystickpressed(joystick,button)
  -- just action attack here, we don't care which button was pressed
  create_attack()
end

function love.joystickreleased(joystick,button)
  -- nothing planned here
end
