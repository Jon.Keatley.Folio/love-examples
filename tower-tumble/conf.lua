function love.conf(t)
	t.window.vsync = 1
	t.window.fullscreen = false
	t.window.width = 960
	t.window.height = 540
	t.identity = "tower_tumble"
	t.window.title = "Tower Tumble"
	

  -- size is expected to have the same aspect ratio as 1080p
	--1920	1080
  --960	540
  --480	270
end
