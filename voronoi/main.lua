
local floor = math.floor
local rect = love.graphics.rectangle

voronoi = love.graphics.newShader [[
    const vec4 point_color = vec4(0,0,0,1);
    const vec2 light = vec2(200,256);
    const float size = 2;

    uniform vec4[100] colors;
    uniform vec2[101] points;
  
	vec4 effect(vec4 global_color, Image texture, vec2 texture_coords, vec2 pixel_coords)
    {
		int index = -1;
		
		float dist = -1;
		
		for(int x=0;x<101;x++)
		{
			if(points[x].x == -1)
			{
				break;
			}
		
			//draw point
			if(pixel_coords.x <= points[x].x + size && pixel_coords.x >= points[x].x - size)
			{
				if(pixel_coords.y <= points[x].y + size && pixel_coords.y >= points[x].y - size)
				{
					return point_color;
				}
			}
		
			float d = distance(pixel_coords,points[x]);			
			if(d < dist || dist == -1)
			{
				dist = d;
				index = x;
			}
		}
		
		if(index == -1)
		{
			return global_color;
		}
		else
		{
			return colors[index]; 
		}
    }
]]

local screensize = love.graphics.getWidth()

local points = {};
local colors = {};

function love.load()
	for i=1,101 do
		colors[i] = {math.random(),math.random(),math.random(),1};
	end
	
	voronoi:send("colors",unpack(colors));
	
	local p = {};
	--clear points
	for i=1,101 do
		p[i] = {-1,-1};
	end
	
	voronoi:send("points",unpack(p));
end


function love.draw()
	love.graphics.setShader(voronoi)
	love.graphics.setColor(0,255,255);
	love.graphics.rectangle("fill", 0,0,512,512)
end

function love.mousereleased(x,y,button,isTouch)
	if button == 1 and #points <= 100 then
		local p = {x,y};
		points[#points + 1] = p;
		
		--handle off by one bug with push
		local ps = {};
		for i=1,#points do
			ps[i] = points[i];
		end
		
		ps[#ps + 1] = {-1,-1};
				
		--push changes to shader
		voronoi:send("points",unpack(ps));
	
	end


end
