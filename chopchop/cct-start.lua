function createCCTStart()

	local mIsSpaceDown = false;
	local s = {};
	
	local AXE_SPEED = 400;

------------------------------------------------------------------------
	s.loader = function()
	
		local pFont = love.graphics.newImage('images/text.png');
		pFont:setFilter("nearest","nearest");
		
		mAxe = newSprite(pFont,80,95,30,30);
		mAxe.AddFrame(2,27 ,300,1);
		mAxe.SetAnimation(1);
		
		mLogo = newSprite(pFont,29,17,5,100);
		mLogo.AddFrame(82,21 ,300,0);
		mLogo.SetAnimation(0);
		
		mPoints = {};
		
		mPoints[#mPoints + 1] = {X=10,Y=44};
		mPoints[#mPoints + 1] = {X=25,Y=32};
		
		mTarget = math.random(1,#mPoints);

		mTimber = newSprite(pFont,41,120,100,0);
		mTimber.AddFrame(119,0 ,300,1);
		mTimber.SetAnimation(1);
		
		local pBg = love.graphics.newImage('images/background.png');
		pBg:setFilter("nearest", "nearest");
		
		mBackground = createParallaxBackground(pBg,pBg:getWidth(),pBg:getHeight(),12);
		mBackground.AddSection(0,29,0)
		mBackground.AddSection(29,43,0)
		mBackground.AddSection(72,17,0)
		mBackground.AddSection(89,20,0)	
		
		local pMisc = love.graphics.newImage('images/misc.png');
		pMisc:setFilter("nearest","nearest");
		
		--setup keys
		mSpaceKey = newSprite(pMisc,20,12,70,105);
		mSpaceKey.AddFrame(8,0,300,0);
		mSpaceKey.AddFrame(8,12,300);
		mSpaceKey.SetAnimation(0);
	end
------------------------------------------------------------------------
	s.reset = function()

		
	end
------------------------------------------------------------------------	
	s.updater = function(_dt,_self)
	
		mSpaceKey.Update(_dt);
	
		
		if mPoints[mTarget].X > mAxe.X then
			mAxe.VolX = AXE_SPEED * _dt;
		elseif mPoints[mTarget].X < mAxe.X then		
			if mAxe.VolX > 0 then
				mAxe.X = mPoints[mTarget].X;
				mAxe.VolX = 0;
			else
				mAxe.VolX = -(AXE_SPEED * _dt);
			end
		else
			mAxe.VolX = 0;
		end
		
		if mPoints[mTarget].Y > mAxe.Y then
			mAxe.VolY = AXE_SPEED * _dt;
		elseif mPoints[mTarget].Y < mAxe.Y then
			if mAxe.VolY > 0 then
				mAxe.Y = mPoints[mTarget].Y;
				mAxe.VolY = 0;
			else
				mAxe.VolY = -(AXE_SPEED * _dt);
			end
		else
			mAxe.VolY = 0;
		end
		
		--print(tostring(mAxe.VolX) .. " " .. tostring(mAxe.VolY));
		
		if (mAxe.VolY == 0 and mAxe.VolX == 0) then
			mTarget = math.random(1,#mPoints);
		end
		
		mBackground.Update(_dt);
		mAxe.Update(_dt);
		mTimber.Update(_dt);
		
		if mIsSpaceDown then
				if not love.keyboard.isDown("space") then
					mIsSpaceDown = false;
					_self.nextScene = 2;
					_self.hasEnded = true;
				end
		elseif love.keyboard.isDown("space") then
				mIsSpaceDown = true;
		end
	end
------------------------------------------------------------------------	
	s.render = function(_width,_height,_self)
	
		mBackground.Draw();
		mTimber.Draw();
		mAxe.Draw();
		mSpaceKey.Draw();
		mLogo.Draw();
	end
------------------------------------------------------------------------

	return createScene(makeColor(0, 0.64, 1),s.loader,s.updater,s.render,s.reset);
end
