
function love.load()

--load framework
	require("joncorp/game");
	GGameEngine = JonCorpGame(160,120);
	
--setup game
	require("cct-start");
	require("cct-game");
	require("cct-end");
	
--add scenes
	GGameEngine.AddScene(1,createCCTStart());
	GGameEngine.AddScene(2,createCCTGame());
	GGameEngine.AddScene(3,createCCTEnd());
	
	GGameEngine.SelectScene(1);

--load scenes
	GGameEngine.Load();

end

function love.update(dt)
	GGameEngine.Update(dt);
end

function love.draw()
	GGameEngine.Draw();
end

