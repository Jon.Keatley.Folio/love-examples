function createCCTEnd()

	local mIsSpaceDown = false;
	local s = {};
	
	local STEPS = 5;
	local TOP = STEPS * 12;
	
	local mBg = nil;
	local mFont = nil;
	local mSpaceKey = nil;
	local mNodes = {};
	local mSections = {};
	local mScore = -1;
	local mProgress = STEPS;
	local mProgressDelay = 1;
	local mIsProgressing = true;
	local mNextTarget = 3;
	local mPos = 1;

------------------------------------------------------------------------
	s.loader = function()
		
		local pBg = love.graphics.newImage('images/end-background.png');
		pBg:setFilter("nearest", "nearest");
		
		local pProg = love.graphics.newImage('images/end-progress.png');
		pBg:setFilter("nearest", "nearest");
		
		local pFont = love.graphics.newImage('images/endtxt.png');
		pFont:setFilter("nearest","nearest");
		
		local pMisc = love.graphics.newImage('images/misc.png');
		pMisc:setFilter("nearest","nearest");
		
		mBg = createImage(pBg,0,0,pBg:getWidth(),pBg:getHeight());
		
		local n = {};
		n.Image = createImage(pProg,0,0,28,23);
		n.Offset = 0;
		mNodes[#mNodes + 1] = n;
		
		local n = {};
		n.Image = createImage(pProg,28,0,28,23);
		n.Offset = 0;
		mNodes[#mNodes + 1] = n;
		
		local n = {};
		n.Image = createImage(pProg,56,0,30,24);
		n.Offset = 1;
		mNodes[#mNodes + 1] = n;
		
		local n = {};
		n.Image = createImage(pProg,0,90,30,30);
		n.Offset = 7;
		mNodes[#mNodes + 1] = n;
		
		local n = {};
		n.Image = createImage(pProg,30,69,30,51);
		n.Offset = 28;
		mNodes[#mNodes + 1] = n;
		
		local n = {};
		n.Image = createImage(pProg,60,80,30,40);
		n.Offset = 17;
		mNodes[#mNodes + 1] = n;
		
		local n = {};
		n.Image = createImage(pProg,90,47,30,73);
		n.Offset = 50;
		mNodes[#mNodes + 1] = n;
		
		local n = {};
		n.Image = createImage(pProg,120,32,30,88);
		n.Offset = 65;
		mNodes[#mNodes + 1] = n;
		
		
		local s = 0;
		local nextPos = 4;
		for s= 1,6 do
			mSections[#mSections + 1] = {node=1,pos=nextPos};
			nextPos = nextPos + 24;
		end
		
		mSpaceKey = newSprite(pMisc,20,12,70,105);
		mSpaceKey.AddFrame(8,0,300,0);
		mSpaceKey.AddFrame(8,12,300);
		mSpaceKey.SetAnimation(0);
		
		--font
		mFont = createFont(pFont,0,0,"1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ., ",12,14,1);
	end
------------------------------------------------------------------------
	s.reset = function()
		
		for sn= 1,6 do
			mSections[sn].node=1;
		end
		
		mScore = -1;
		mProgress = STEPS;
		mProgressDelay = 1;
		mIsProgressing = true;
		mNextTarget = 3;
		mPos = 1;
	end
------------------------------------------------------------------------	
	s.updater = function(_dt,_self)
		mSpaceKey.Update(_dt);
		if mScore == -1 then
			mScore = _self.GameData.GetValue("score") or -1;
			
			print("Found score " .. mScore .. " out of " .. TOP);
			
			if mScore == 0 then
				mIsProgressing = false;
			end
		end

		if mIsProgressing then
			mProgressDelay = mProgressDelay - _dt;
			
			if mProgressDelay < 0 then
				mProgressDelay = 1;
				mSections[mPos].node = mSections[mPos].node + 1;
				if mSections[mPos].node > 2 then
					mSections[mPos].node = mNextTarget;
					mNextTarget = mNextTarget + 1;
					mPos = mPos + 1;
					if mPos > 6 then
						mIsProgressing = false;
					end
				end

				mProgress = mProgress + STEPS;
				if mProgress > mScore then
					mIsProgressing = false;
				end
			end
		else
			if mIsSpaceDown then
				if not love.keyboard.isDown("space") then
					mIsSpaceDown = false;
					_self.nextScene = 1;
					_self.hasEnded = true;
				end
			elseif love.keyboard.isDown("space") then
				mIsSpaceDown = true;
			end
		end
	end
------------------------------------------------------------------------	
	s.render = function(_width,_height,_self)
	
		mBg.Draw(0,0);
		
		for k,v in pairs(mSections) do
			mNodes[v.node].Image.Draw(v.pos,95 - mNodes[v.node].Offset);
		end
		
		local px = 2;
		
		if mIsProgressing then
			mFont.Draw(px,5,"progress");
		else
			mFont.Draw(px,5,"Result");
			
			if mScore >= 60 then
				mFont.Draw(px,20,"lord lumber");
			elseif mScore > 40 then
				mFont.Draw(px,20,"axe man");
			elseif mScore > 20 then
				mFont.Draw(px,20,"chopper");
			elseif mScore == 0 then
				mFont.Draw(px,20,"Tree Huger");
			else
				mFont.Draw(px,20,"Celery arms");
			end
			
			mSpaceKey.Draw();
		end

	end
------------------------------------------------------------------------

	return createScene(makeColor(0, 255, 0),s.loader,s.updater,s.render,s.reset);
end
