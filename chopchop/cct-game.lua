function createCCTGame()

	local TREE_SPEED = 18;
	local s = {};
	local mPlayer = nil;
	local mSpaceKey = nil;
	local mBackground = nil;
	local mTrees = nil;
	local mTarget = nil;
	local mMeter = nil;
	local mEffects = nil;
	local mSpriteBatch = nil;
	local mTimer = nil;
	local mHitInfo = nil;
	local mChopChop = nil;
	local mMode = 0;
	local mNextMode = 0;
	local mPlayerPosX = 50;
	local mPlayerPosMod = 0;
	local mIsSpaceDown = false;
	local mTreesFell = 0;
	
	local mSpeedOffset = 0;
	local mSpeedBoost = 1;
	local mNextSpeedTarget = 40;
	local mMeterSpeed = 20;
	
	
	local mMaxTrees = 6;
	local mTreeCoolDown = 0;
	
------------------------------------------------------------------------
	s.loader = function()
		require("power");
		require("trees");
		require("target");
		require("timer");
		require("hitinfo");
	
		local pImg = love.graphics.newImage('images/chopchopjack.png');
		pImg:setFilter("nearest", "nearest");
		
		local pBg = love.graphics.newImage('images/background.png');
		pBg:setFilter("nearest", "nearest");
		
		local pMisc = love.graphics.newImage('images/misc.png');
		pMisc:setFilter("nearest","nearest");
		
		local pFont = love.graphics.newImage('images/text.png');
		pFont:setFilter("nearest","nearest");
		
		mTimer = createTimer(pFont,pFont,80,2)
		
		--setup power meter
		mMeter = createPowerMeter(pMisc,56,37,8,37,0,0,1,makeColor(142, 214, 255));
		mMeter.SetVol(mMeterSpeed);
		
		--setup target
		mTarget = newTarget(pMisc,24,24,82,75,15);
		mTarget.SetVol(mMeterSpeed);
		
		--setup trees
		mTrees = createTrees(pMisc,112,28,31,0,160);
		
		mTrees.AddTreePart(48 ,89,3);
		mTrees.AddTreePart(76 ,89,3);
		mTrees.AddTreePart(104,89,3);
		
		mTrees.AddTreePart(132,89,2);
		mTrees.AddTreePart(48 ,58,2);
		mTrees.AddTreePart(104,58,2);
		mTrees.AddTreePart(132,58,2);
		
		mTrees.AddTreePart(76 ,58,1);
		mTrees.AddTreePart(104,27,1);
		mTrees.AddTreePart(132,27,1);
		
		mTrees.AddStartTrees();
		mTrees.SetSpeed(TREE_SPEED);
		
		--chop chop message
		mChopChop = newSprite(pFont,80,95,-80,10);
		mChopChop.AddFrame(2,27 ,300,1);
		mChopChop.SetAnimation(1);
		
		--setup player	
		local pw,ph = 32,35;
		mPlayer = newSprite(pImg,pw,ph,mPlayerPosX,77);
		
--- normal speed
		mPlayer.AddFrame(0 + (pw * 3),0 ,300,1); --standing
				
		mPlayer.AddFrame(0           ,0 ,300,2); -- walking
		mPlayer.AddFrame(0 + (pw    ),0 ,200  );
		mPlayer.AddFrame(0 + (pw * 2),0 ,300  );
		mPlayer.AddFrame(0 + (pw    ),0 ,200  );
		
		mPlayer.AddFrame(0           ,70,300,3); -- aiming
		mPlayer.AddFrame(0 + (pw    ),70,300  );
		
		mPlayer.AddFrame(0           ,35,300,4,false); --attack
		mPlayer.AddFrame(0 + (pw    ),35,100  );
		mPlayer.AddFrame(0 + (pw * 2),35,400  );
--- mid speed
		mPlayer.AddFrame(0 + (pw * 3),0 ,300,5);--standing
				
		mPlayer.AddFrame(0           ,0 ,200,6);-- walking
		mPlayer.AddFrame(0 + (pw    ),0 ,100  );
		mPlayer.AddFrame(0 + (pw * 2),0 ,200  );
		mPlayer.AddFrame(0 + (pw    ),0 ,100  );
		
		mPlayer.AddFrame(0           ,70,200,7);-- aiming
		mPlayer.AddFrame(0 + (pw    ),70,200  );
		
		mPlayer.AddFrame(0           ,35,200,8,false);--attack
		mPlayer.AddFrame(0 + (pw    ),35,50  );
		mPlayer.AddFrame(0 + (pw * 2),35,300  );
--fast speed
		mPlayer.AddFrame(0 + (pw * 3),0 ,300,9);--standing
				
		mPlayer.AddFrame(0           ,0 ,100,10);-- walking
		mPlayer.AddFrame(0 + (pw    ),0 ,50  );
		mPlayer.AddFrame(0 + (pw * 2),0 ,100  );
		mPlayer.AddFrame(0 + (pw    ),0 ,50  );
		
		mPlayer.AddFrame(0           ,70,100,11);-- aiming
		mPlayer.AddFrame(0 + (pw    ),70,100  );
		
		mPlayer.AddFrame(0           ,35,100,12,false);--attack
		mPlayer.AddFrame(0 + (pw    ),35,25  );
		mPlayer.AddFrame(0 + (pw * 2),35,200  );
		
		mPlayer.SetAnimation(2);
		
		--setup keys
		mSpaceKey = newSprite(pMisc,20,12,68,50);
		mSpaceKey.AddFrame(8,0,300,0);
		mSpaceKey.AddFrame(8,12,300);
		mSpaceKey.SetAnimation(0);
		
		--setup parallax scrolling background
		mBackground = createParallaxBackground(pBg,pBg:getWidth(),pBg:getHeight());
		mBackground.AddSection(0,29,0)
		mBackground.AddSection(29,43,-2)
		mBackground.AddSection(72,17,-6)
		mBackground.AddSection(89,20,-9)
		mBackground.AddSection(109,11,-TREE_SPEED) --14
		
		--setup sprite batch
		mSpriteBatch = newSpriteBatch(pMisc,pMisc:getWidth(),pMisc:getHeight());
		mSpriteBatch.AddAnimation(0,40,29,26,100,1);
		mSpriteBatch.AddAnimation(0,66,29,26,100);
		mSpriteBatch.AddAnimation(0,92,29,26,200);
		mSpriteBatch.AddAnimation(75,1,29,26,100);
		mSpriteBatch.AddAnimation(104,1,29,26,100);
		
		--setup effects
		mEffects = newParticalEngine(pMisc,99,3,50,20);
		mEffects.AddTemplate(57 ,95,10,11);
		mEffects.AddTemplate(56 ,88,10,11);
		mEffects.AddTemplate(85 ,89,10,11);
		mEffects.AddTemplate(113,72,10,11);
		
		--set up hits
		mHitInfo = createHitInfo(pFont,mPlayerPosX + 30,80,2,10);
		
	end
------------------------------------------------------------------------	
	s.updater = function(_dt,_self)
		
		mPlayer.Update(_dt);
		mEffects.Update(_dt);
		mSpriteBatch.Update(_dt);
		mTimer.Update(_dt);
		mHitInfo.Update(_dt);
		mTarget.MoveTarget(_dt);
		mChopChop.Update(_dt);
		if mChopChop.X > 160 then
			mChopChop.X = -80;
			mChopChop.VolX = 0;
		end
		
		if mTimer.GetIntTime()  < mNextSpeedTarget then
			mSpeedOffset = mSpeedOffset + 4;
			mSpeedBoost = mSpeedBoost + 1;
			
			mMeterSpeed = mMeterSpeed + 20;
			mTarget.SetVol(mMeterSpeed);
			mMeter.SetVol(mMeterSpeed);
			
			mChopChop.VolX = 100;
			
			local s = mSpeedBoost * TREE_SPEED;
			mBackground.SetSectionSpeed(-s,5);
			mTrees.SetSpeed(s);
			
			if mNextSpeedTarget > 0 then
				mNextSpeedTarget = mNextSpeedTarget - 20;
			end
		end
		
		if mTimer.HasEnded() then --game over
			_self.nextScene = 3;
			_self.hasEnded = true;
			_self.GameData.AddValue("score",mTreesFell * 3);
		end

		if love.keyboard.isDown('escape') then
			_self.nextScene = -1;
			_self.hasEnded = true;
		end
		
		if mMode == 0 then
			mBackground.Update(_dt);
			mTrees.Update(_dt);
			
			if mPlayer.VolX < 0 then
				if mPlayer.X < mPlayerPosX then
					mPlayer.X = mPlayerPosX;
					mPlayer.VolX = 0;
				end
			end

			if mTrees.CountTrees() > 0 then
				if mTrees.NearestTree() <= 80 then
					mMode = 1
					mPlayer.VolX = 0
					mPlayer.SetAnimation(3 + mSpeedOffset);
					mIsSpaceDown = true;
				end
			end
		elseif mMode == 1 then	
			mMeter.Update(_dt)
			mSpaceKey.Update(_dt);

			if mIsSpaceDown then
				if not love.keyboard.isDown("space") then
					mIsSpaceDown = false;
				end
			elseif love.keyboard.isDown("space") then
				mPlayer.X = mPlayerPosX;
				mMode = 2;
				mIsSpaceDown = true;
			end
		elseif mMode == 2 then
			mSpaceKey.Update(_dt);
			
			if mIsSpaceDown then
				if not love.keyboard.isDown("space") then
					mIsSpaceDown = false;
				end
			elseif love.keyboard.isDown("space") then
			
				mPlayer.SetAnimation(4 + mSpeedOffset);
				mPlayer.X = mPlayerPosX + 10;
				mMode = 3;
			
				local hit = math.ceil(mTarget.GetHitPercent() * (mMeter.GetLevel() / 100));
				
				if hit > 15 then
					local result = mTrees.TreeHit(hit);
					
					mHitInfo.AddHit(tostring(hit));
					
					if result == 2 then --destroyed tree
						mNextMode = 0;
						mTreesFell = mTreesFell + 1;
					elseif result == 1 then -- damaged tree
						mNextMode = 1;
					end
				else 
					--missed the tree
					mHitInfo.AddHit("MISS");
					mNextMode = 1;
				end
				
				mIsSpaceDown = true;
				mMeter.Reset();
			
			end
		elseif mMode == 3 then
		
			if mNextMode == 0 then
				if mPlayer.CurrentFrame == 2 then
					local treeLength = mTrees.CurrentTreeSize();
					local pos = 80;
					for l = 1,treeLength do
						mSpriteBatch.AddSprite(80,pos,1);
						pos = pos - 30;
					end

				end
			end
		
			if mPlayer.AnimationStopped then
				mMode = mNextMode;

				if mMode == 0 then
					mPlayer.SetAnimation(2 + mSpeedOffset);
					mPlayer.VolX = -30;
					local tsize = mTrees.CurrentTreeSize();
					mEffects.AddParticals(70,80 - (tsize * 30),80,100,4,2,tsize * 2);
					mTrees.KillTree();
				else
					mPlayer.SetAnimation(3 + mSpeedOffset);
					mPlayer.VolX = 0;
					mPlayer.X = mPlayerPosX;
				end
			end
		end
	end
------------------------------------------------------------------------	
	s.render = function(_width,_height,_self)
	
		mBackground.Draw();
		
		if mChopChop.VolX > 0 then
			mChopChop.Draw();
		end
		
		mTrees.Draw();
		
		if mMode == 1 then
			mMeter.Draw();
			mSpaceKey.Draw();

		elseif mMode == 2 then
			mMeter.Draw();
			mSpaceKey.Draw();
			mTarget.Draw();
		end
		
		mPlayer.Draw();
		
		mHitInfo.Draw();
		mEffects.Draw();
		mSpriteBatch.Draw();
		
		mTimer.Draw();
	end
	
------------------------------------------------------------------------
	s.reset = function()
		mTimer.Reset();
		mMode = 0;
		mTrees.ClearAll();
		mTrees.AddStartTrees();
		mPlayer.SetAnimation(2);
		mPlayer.X = mPlayerPosX;
		mNextMode = 0;
		mPlayerPosMod = 0
		mIsSpaceDown = false
		mTreeCoolDown = 0;
		mTreesFell = 0;
		mEffects.Reset();
		mSpriteBatch.Reset();
		mHitInfo.Reset();	
		mSpeedOffset = 0;
		mNextSpeedTarget = 40;
		mTrees.SetSpeed(TREE_SPEED);
		mSpeedBoost =1;
		mBackground.SetSectionSpeed(-TREE_SPEED,5);
		mMeterSpeed = 20;
		mTarget.SetVol(mMeterSpeed);
		mMeter.SetVol(mMeterSpeed);
	end
------------------------------------------------------------------------

	return createScene(makeColor(142, 214, 255),s.loader,s.updater,s.render,s.reset);
end
