function createPowerMeter(_img,_x,_y,_width,_height,_dx,_dy,_border,_fillcolor)

	local p = {};
	
	p.image = _img;
	p.x = _x;
	p.y = _y;
	p.level = 0;
	p.width = _width;
	p.height = _height;
	p.border = _border;
	p.bounds = love.graphics.newQuad(_dx,_dy,_width,_height,_img:getWidth(),_img:getHeight());
	p.color = _fillcolor;
	p.overlayX = _x + _border;
	p.overlayY = _y + _border;
	p.overlayWidth = _width - (_border * 2);
	p.overlayHeight = _height -( _border * 2);
	p.mod = 6;

------------------------------------------------------------------------
	p.Draw = function()
		love.graphics.draw(p.image,p.bounds, p.x,p.y);
		love.graphics.setColor(p.color.R,p.color.G,p.color.B,255);
		love.graphics.rectangle("fill",p.overlayX,p.overlayY,p.overlayWidth,p.overlayHeight);
		love.graphics.setColor(255,255,255,255);	
	end
	
------------------------------------------------------------------------
	p.SetVol = function(_vol)
		p.mod = 60 + (_vol * 4);
	end
------------------------------------------------------------------------
	p.Reset = function()
		p.level = 0;
		if p.mod < 0 then
			p.mod = -p.mod;
		end
	end

------------------------------------------------------------------------

	p.Update = function(_dt)
		p.level = p.level + (p.mod * _dt);
		
		if p.level >= 100 then
			p.level = 100;
			p.mod = -p.mod;
		elseif p.level <= 0 then
			p.level = 0;
			p.mod = -p.mod;
		end
		

		
		p.SetLevel(p.level);
	end
	
------------------------------------------------------------------------
	p.GetLevel = function()
		return p.level;
	end
	
------------------------------------------------------------------------
	p.SetLevel = function(_lvl)
		if _lvl < 0 then
			_lvl = 0;
		end
		
		if _lvl > 100 then
			_lvl = 100;
		end
	
		p.level = _lvl;
		p.overlayHeight = ((p.height / 100) * (100 - _lvl)) - (p.border * 2);
		
		if p.overlayHeight < 0 then
			p.overlayHeight = 0;
		end
		
		
		--p.overlayY = (p.y + (p.height - p.overlayHeight));
		
	end
	
------------------------------------------------------------------------
	return p;
end
