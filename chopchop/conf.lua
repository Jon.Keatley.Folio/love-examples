-- Configuration
function love.conf(t)
	t.title = "Chop-Chop-Timber" 
	t.version = "11.1"         
	t.window.width = 640
	t.window.height = 480
	t.window.fullscreen = false

-- qvga - 320 x 240
-- vga  - 640 x 480

	-- For Windows debugging
	t.console = true
end
