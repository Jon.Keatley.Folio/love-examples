function createTimer(_imgLarge,_imgSmall,_x,_y)

	local t = {};
	
	t.LargeFont = createFont(_imgLarge,0,7,"1234567890",12,14,2);
	t.SmallFont = createFont(_imgSmall,0,0,"1234567890",6,7,1);
	t.Timer = 60.00;
	t.TimerHigh = "60";
	t.TimerLow  = "00";
	t.X = _x - 17;
	t.Y = _y;
	
	t.GetIntTime = function()
		return math.floor(t.Timer);
	end
	
	t.Reset = function()
		t.Timer = 60.00;
	end
	
	t.Update = function(_dt)
	
		t.Timer = t.Timer - _dt;
		
		if t.Timer < 0 then
			t.Timer = 0;
		end
		
		local f = string.format("%0.2f", t.Timer);
		if #f == 4 then
			f= "0" .. f;
		end
		t.TimerHigh = string.sub(f,1, 2);
		t.TimerLow = string.sub(f,4, 5);	
	end
	
	t.Draw = function()
	
		t.LargeFont.Draw(t.X,t.Y,t.TimerHigh);
		t.SmallFont.Draw(t.X + 26,t.Y + 7,t.TimerLow);
	end
	
	t.HasEnded = function()
		return t.Timer <= 0;
	end
	
	
	return t;
	
end
