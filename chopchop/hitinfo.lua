function createHitInfo(_imgSmall,_x,_y,_life,_vol)

	local t = {};
	t.SmallFont = createFont(_imgSmall,0,0,"1234567890MIS",6,7,0);
	t.X = _x;
	t.Y = _y;
	t.MaxLife = _life;
	t.Vol = _vol
	t.Hits = {};
	
------------------------------------------------------------------------
	
	t.Reset = function()
		t.Hits = {};
	
	end
	
------------------------------------------------------------------------
	
	t.AddHit = function(_txt)
		local n = {};
		n.Text = _txt;
		n.Y = t.Y;
		n.Life = 0;
		
		t.Hits[#t.Hits + 1] = n;
	end	
------------------------------------------------------------------------

	t.Update = function(_dt)
	
		for k,v in pairs(t.Hits) do
		
			v.Life = v.Life + _dt;
			if v.Life >= t.MaxLife then
				table.remove(t.Hits,k);
			end
			
			v.Y = v.Y - (_dt * t.Vol);
		end
	
	end
------------------------------------------------------------------------

	t.Draw = function()
		for k,v in pairs(t.Hits) do

			t.SmallFont.Draw(t.X,v.Y,v.Text);
		end
	
	end
------------------------------------------------------------------------
	return t;
end
