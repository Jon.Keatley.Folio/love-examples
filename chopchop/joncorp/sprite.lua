-- sprite creation functions -----------------------------------------------

function newSprite(_image,_w,_h,_x,_y)

	local ns = {}
	ns.Image = _image;
	ns.Width = _w;
	ns.Height = _h;
	ns.X = _x;
	ns.Y = _y;
	ns.VolX = 0;
	ns.VolY = 0;
	ns.CurrentFrame = 1;
	ns.Timer = 0;
	ns.AnimationId = 1;
	ns.Animations = {};
	ns.AnimationStopped = false;
	ns.ImgW = _image:getWidth();
	ns.ImgH = _image:getHeight();

------------------------------------------------------------------------
	ns.AddFrame = function(_x,_y,_delay,_id,_repeat)
	
		if _id then
			
			if _repeat ~= false then
				_repeat = true
			end
		
			ns.AnimationId = _id;
			ns.Animations[_id] = {};
			ns.Animations[_id].f = {};
			ns.Animations[_id].FrameCount = 0;	
			ns.Animations[_id].loop = _repeat;
		else
			_id = ns.AnimationId;
		end		
		
		ns.Animations[_id].FrameCount = ns.Animations[_id].FrameCount + 1;
		ns.Animations[_id].f[ns.Animations[_id].FrameCount] = {};
		ns.Animations[_id].f[ns.Animations[_id].FrameCount].Frame = love.graphics.newQuad(_x,_y,ns.Width,ns.Height,ns.ImgW,ns.ImgH);
		ns.Animations[_id].f[ns.Animations[_id].FrameCount].Delay = _delay;
	end
------------------------------------------------------------------------
	ns.Update = function(_dt)

		ns.X = ns.X + (ns.VolX * _dt);
		ns.Y = ns.Y + (ns.VolY * _dt);
	
		local a = ns.AnimationId
		local cf = ns.CurrentFrame
		
		if(ns.Animations[a].FrameCount > 1) then
			ns.Timer = ns.Timer + (_dt * 1000);
			if ns.Timer >= ns.Animations[a].f[cf].Delay then
				ns.Timer = ns.Timer - ns.Animations[a].f[cf].Delay;
				cf = cf + 1;
				if cf > ns.Animations[a].FrameCount then
					if not ns.Animations[a].loop then
						cf = cf - 1
						ns.AnimationStopped = true;
					else
						cf = 1;
					end			
				end
				
				ns.CurrentFrame = cf
			end
		end
	end
------------------------------------------------------------------------
	ns.Draw = function()
		love.graphics.draw(ns.Image,ns.Animations[ns.AnimationId].f[ns.CurrentFrame].Frame, ns.X, ns.Y);
	end
------------------------------------------------------------------------

	ns.SetAnimation = function(_id)
		if ns.Animations[_id] then
			ns.CurrentFrame = 1;
			ns.Timer = 0;
			ns.AnimationStopped = false;
			ns.AnimationId = _id;
		end
	end

	return ns;
end
