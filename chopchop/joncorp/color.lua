function makeColor(_r,_g,_b,_a)

	local c = {};
	c.R = _r;
	c.G = _g;
	c.B = _b;
	
	c.A = _a or 1;
	
	return c;
end
