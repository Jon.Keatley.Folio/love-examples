
function JonCorpGame(_width,_height)

	-- load framework
	require("joncorp/scene")
	require("joncorp/sprite")
	require("joncorp/color")
	require("joncorp/images")
	require("joncorp/parallaxBg")
	require("joncorp/particals")
	require("joncorp/spriteBatch")
	require("joncorp/font")
	require("joncorp/gameData")
	
	--create game canvas
	local canvas = love.graphics.newCanvas(_width,_height);
	canvas:setFilter("nearest", "nearest");
	
	local scaleX = love.graphics.getWidth() / _width;
	local scaleY = love.graphics.getHeight() / _height;
	local currentSceneObj = nil;
	local bgColor = nil;
	
	local gameData = createGameData();

	--create game object
	local game = {};
	game.Scenes = {};
	game.CurrentScene = -1;
	
------------------------------------------------------------------------
	game.Load = function()
		for key,s in pairs(game.Scenes) do
			s.Load();
			s.SetData(gameData);
		end	
	end
	
------------------------------------------------------------------------
	game.SelectScene = function(_id)
		if game.Scenes[_id] then
			game.CurrentScene = _id;
			currentSceneObj = game.Scenes[_id];
			bgColor = currentSceneObj.GetBackground();
		end
	end
	
------------------------------------------------------------------------
	game.AddScene = function(_id,_scene)
		game.Scenes[_id] = _scene;
	end
	
------------------------------------------------------------------------
	game.Update = function(dt)
		currentSceneObj.Update(dt);
		
		if currentSceneObj.HasEnded() then
			local id = currentSceneObj.GetNextScene();
			
			if id == -1 then
				-- end game
				love.window.setFullscreen(false);
				love.event.push('quit');
			else
				local oldScene = currentSceneObj;
				game.SelectScene(id);
				oldScene.Reset();
			end
		end
	end
	
------------------------------------------------------------------------
	game.Draw = function()
		love.graphics.setCanvas(canvas);

		love.graphics.setBackgroundColor( bgColor.R, bgColor.G, bgColor.B)
		love.graphics.clear();
		
		currentSceneObj.Draw(_width,_height);
		
		love.graphics.setCanvas() 
		love.graphics.draw(canvas, 0, 0, 0, scaleX, scaleY)
	end

	return game;
end
