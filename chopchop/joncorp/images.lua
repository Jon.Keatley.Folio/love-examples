function createImage(_image,_x,_y,_w,_h)

	local im = {};
	
	im.image = _image;
	im.bounds = love.graphics.newQuad(_x,_y,_w,_h,_image:getWidth(),_image:getHeight());
	im.width = _w;
	im.height = _h;

------------------------------------------------------------------------
	im.Draw = function(x,y)
		love.graphics.draw(im.image,im.bounds, x,y);
	end

------------------------------------------------------------------------
	return im;
end
