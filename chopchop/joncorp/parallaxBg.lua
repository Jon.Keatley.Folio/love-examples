function createParallaxBackground(_image,_w,_h,_start)

	local bg = {};
	
	bg.start = _start or 0;
	bg.image = _image;
	bg.width = _w;
	bg.height = _h;
	bg.sections = {}
	
------------------------------------------------------------------------
	bg.SetSectionSpeed = function(_speed,_section)
		if bg.sections[_section] then
			bg.sections[_section].speed = _speed;
		else
			print(_section .. " not found!");
		end
	end

------------------------------------------------------------------------
	bg.Update = function(_dt)
		for k,v in pairs(bg.sections) do
			v.offset = (v.offset) + (_dt * v.speed)
			
			if v.speed > 0 then
				if v.offset >= bg.width then
					v.offset = v.offset - bg.width;
				end
			else
				if v.offset <= -bg.width then
					v.offset = v.offset + bg.width;
				end
			end
		end
	
	end
	
------------------------------------------------------------------------
	bg.Draw = function()
	
		for k,v in pairs(bg.sections) do
			if v.speed == 0 then
				love.graphics.draw(bg.image,v.rectA, 0 ,v.y);
			else
				love.graphics.draw(bg.image,v.rectA, v.offset ,v.y);
				
				if v.speed > 0 then
					love.graphics.draw(bg.image,v.rectA, v.offset - bg.width ,v.y);
				else
					love.graphics.draw(bg.image,v.rectA, v.offset + bg.width ,v.y);
				end
			end
		end
	
	end

------------------------------------------------------------------------
	bg.AddSection = function(_start,_height,_speed)
		local s = {};
		s.y = _start;
		s.h = _height;
		s.offset = 0;
		s.speed = _speed;
	
		s.rectA = love.graphics.newQuad(0,s.y,bg.width,s.h,bg.width,bg.height);
		bg.sections[#bg.sections + 1] = s;
		
		s.y = s.y + bg.start;
	end
	
------------------------------------------------------------------------
	return bg;
end
