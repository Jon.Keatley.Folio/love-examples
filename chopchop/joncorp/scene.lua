function createScene(_background,_load,_update,_draw,_reset)

	local s = {};
	s.SceneUpdater = _update;
	s.SceneRender  = _draw;
	s.SceneLoader  = _load;
	s.SceneReset   = _reset;
	
	local background = _background;
	s.hasEnded = false;
	s.nextScene = -1;	
------------------------------------------------------------------------
	s.GetBackground = function()
		return background;
	end
------------------------------------------------------------------------
	
	s.Reset = function()
		s.hasEnded = false;
		s.SceneReset();
	end
	
------------------------------------------------------------------------
	s.Load = function()
		s.SceneLoader();
	end
	
------------------------------------------------------------------------
	s.SetData = function(_data)
		s.GameData = _data;
	end
------------------------------------------------------------------------
	s.Update = function(_dt)
		s.SceneUpdater(_dt,s);
	end
	
------------------------------------------------------------------------
	s.HasEnded = function()
		return s.hasEnded;
	end
	
------------------------------------------------------------------------
	s.GetNextScene = function()
		return s.nextScene;
	end
	
------------------------------------------------------------------------
	s.Draw = function(_width,_height)
		s.SceneRender(_width,_height,s);
	end
------------------------------------------------------------------------	
	return s;
end
