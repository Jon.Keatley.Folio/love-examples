function createGameData()

	local gd = {};
	
	local mData = {};
	
	gd.AddValue = function(_key,_value)
		mData[_key] = _value;
	end
	
	gd.GetValue = function(_key)
		return mData[_key] or nil;
	end
	
	gd.RemoveValue = function(_key)
		table.remove(mData,_key);
	end	
	
	return gd;
end
