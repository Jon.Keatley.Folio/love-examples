function newSpriteBatch(_image,_w,_h)

	local ns = {}
	ns.Image = _image;
	ns.Width = _w;
	ns.Height = _h;
	ns.AnimationId = 1;
	ns.Animations = {};
	ns.Sprites = {};
	
	ns.Reset = function()
		ns.Sprites = {};
	end
	
------------------------------------------------------------------------

	ns.AddAnimation = function(_x,_y,_width,_height,_delay,_id)
		local a = {};
		
		if _id then
				
			ns.AnimationId = _id;
			ns.Animations[_id] = {};
			ns.Animations[_id].f = {};
			ns.Animations[_id].FrameCount = 0;	
			ns.Animations[_id].loop = _repeat;
		else
			_id = ns.AnimationId;
		end		
		
		ns.Animations[_id].FrameCount = ns.Animations[_id].FrameCount + 1;
		ns.Animations[_id].f[ns.Animations[_id].FrameCount] = {};
		ns.Animations[_id].f[ns.Animations[_id].FrameCount].Quad = love.graphics.newQuad(_x,_y,_width,_height,ns.Image:getWidth(),ns.Image:getHeight());
		ns.Animations[_id].f[ns.Animations[_id].FrameCount].Delay = _delay;
	
	end
	
------------------------------------------------------------------------

	ns.AddSprite = function(_x,_y,_id)
		
		local a = {};
		
		if ns.Animations[_id] then

			a.Anim = ns.Animations[_id];
			a.X = _x;
			a.Y = _y;
			a.Timer = 0;
			a.Frame = 1;
		
			ns.Sprites[#ns.Sprites + 1] = a;
		else
			print("Animation not found!");
		end
	end
	
------------------------------------------------------------------------

	ns.Update = function(_dt)
	
		for k,v in pairs(ns.Sprites) do
		
			v.Timer = v.Timer + (_dt * 1000);
			
			if v.Timer >= v.Anim.f[v.Frame].Delay then 
				v.Timer = v.Timer - v.Anim.f[v.Frame].Delay;
				v.Frame = v.Frame + 1;
				
				if v.Frame > v.Anim.FrameCount then
					table.remove(ns.Sprites,k);
				end
			end
		end
	end
	
------------------------------------------------------------------------

	ns.Draw = function()
	
		for k,v in pairs(ns.Sprites) do
			love.graphics.draw(ns.Image,v.Anim.f[v.Frame].Quad, v.X, v.Y);
		end
	end
	
------------------------------------------------------------------------

	return ns;

end
