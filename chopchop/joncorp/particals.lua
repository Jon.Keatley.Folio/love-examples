function newParticalEngine(_image,_ground,_gravity,_max,_min)
	local pe = {};
	
	pe.Image =_image;
	
	pe.Templates = {};
	pe.Particales = {};
	
	pe.Gravity = _gravity;
	pe.Ground = _ground;
	pe.MaxVelocity = _max;
	pe.MinVelocity = _min;
	
	pe.Reset = function()
		pe.Particales = {};
	end
	
------------------------------------------------------------------------
	
	pe.AddTemplate = function(_x,_y,_width,_height)
		pe.Templates[#pe.Templates + 1] = love.graphics.newQuad(_x,_y,_width,_height,pe.Image:getWidth(),pe.Image:getHeight());
	end

------------------------------------------------------------------------

	pe.AddParticals = function(_left,_top,_bottom,_right,_max,_min,_total)
	
		for x = 1, _total do
			local p = {};
			p.Template = math.random(1,#pe.Templates);
			p.X = math.random(_left,_right);
			p.Y = math.random(_top,_bottom);
			p.VolY = -math.random(pe.MinVelocity ,pe.MaxVelocity); 
			p.VolX = math.random(-pe.MaxVelocity,pe.MaxVelocity);
			p.Life = math.random(_min,_max);
			
			pe.Particales[#pe.Particales + 1] = p;
		end
	end
------------------------------------------------------------------------
	pe.Update = function(_dt)
	
		for k,v in pairs(pe.Particales) do
			v.X = v.X + (v.VolX * _dt);
			v.Y = v.Y + (v.VolY * _dt);
			
			v.VolY = v.VolY + pe.Gravity
			
			if v.Y > pe.Ground then
				v.Y = pe.Ground;
				v.VolY = -(v.VolY / 1.5);
			end
			
			v.Life = v.Life - _dt;
			
			if v.Life < 0 then
				table.remove(pe.Particales,k);
			end
		end
	end
	
------------------------------------------------------------------------

	pe.Draw = function()
	
		for k,v in pairs(pe.Particales) do
			love.graphics.draw(pe.Image,pe.Templates[v.Template], v.X, v.Y);
		end

	end
	
	return pe;
end
