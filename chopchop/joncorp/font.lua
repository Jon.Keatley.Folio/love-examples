function createFont(_img,_offX,_offY,_chars,_charWidth,_charHeight,_spacing)

	local f = {};
	
	f.Image = _img;
	f.Characters = {};
	f.Width = _charWidth;
	f.Height = _charHeight;
	
	f.Spacing = _spacing or 0;
	
	local posX,posY = _offX,_offY;
	
	for i = 1, #_chars do
		local c = _chars:sub(i,i)
		f.Characters[c] = love.graphics.newQuad(posX,posY,f.Width,f.Height,f.Image:getWidth(),f.Image:getHeight());
		posX = posX + f.Width;
		if posX >= f.Image:getWidth() then
			posX = 0;
			posY = posY + f.Height;
		end
	end
		
------------------------------------------------------------------------
	
	f.Draw = function(_x,_y,_txt)
		local posX = _x;
		_txt = string.upper(_txt);
		for i = 1, #_txt do
			local c = _txt:sub(i,i)
			
			if c == " " then
				posX = posX +  (f.Width / 4);
			else
				if f.Characters[c] then
					love.graphics.draw(f.Image,f.Characters[c], posX, _y);
					posX = posX + f.Width + f.Spacing;
				else
					print("Font Missing Char " .. c);
				end
			end
		end
	
	end

------------------------------------------------------------------------
	
	return f;
	
end
