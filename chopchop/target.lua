function newTarget(_image,_w,_h,_x,_y,_movement)

	local t = newSprite(_image,_w,_h,_x,_y);
	
	t.VolX = 0;
	t.leftBound = _x - _movement;
	t.rightBound = _x + _movement;
	t.FullHit = _x;
	t.Movement = _movement;
	t.AddFrame(28,24,300,0);
	t.AddFrame(28, 0,300,1);
	t.AddFrame(52,24,300,2);
	t.SetAnimation(0);
	
------------------------------------------------------------------------

	t.SetVol = function(_vol)
		t.VolX = _vol;
	end

------------------------------------------------------------------------	
	
	t.MoveTarget = function(_dt)
	
		if t.X <= t.leftBound then
			t.VolX = -t.VolX;
			t.X = t.leftBound;
		elseif t.X >= t.rightBound then
			t.VolX = -t.VolX;
			t.X = t.rightBound;
		end
		
		local hit = t.GetHitPercent();
		
		if hit > 70 then
		 t.SetAnimation(2);
		elseif hit > 30 then
		 t.SetAnimation(1);
		else
		 t.SetAnimation(0);
		end
		
		t.Update(_dt);
	end
	
------------------------------------------------------------------------
	
	t.GetHitPercent = function()
		local diff = t.FullHit - t.X;
		
		if diff < 0 then
			diff = -diff;
		end
		
		return 100 - ((diff / t.Movement) * 100);
	end

	return t;
end
