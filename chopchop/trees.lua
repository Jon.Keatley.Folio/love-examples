function createTrees(_img,_y,_width,_height,_boundsLeft,_boundsRight)

	local treeSteps = 30;
	local t = {};
	
	t.trees = {};
	
	t.tops = {};
	t.mids = {};
	t.bottoms = {};
	
	t.speed = 0;
	
	t.image = _img;
	t.boundsLeft  = _boundsLeft;
	t.boundsRight = _boundsRight;
	t.nextTreeMarker = _boundsRight - treeSteps;
	t.width  = _width;
	t.height = _height;
	t.ground = _y;
	
	t.level = 1;
	t.killed = 1;

------------------------------------------------------------------------
	t.SetSpeed = function(_speed)
		t.speed = _speed;
	end

------------------------------------------------------------------------
	
	t.ClearAll = function()
		t.trees = {};
		t.level = 1;
		t.killed = 1;
	end
	
------------------------------------------------------------------------

	t.AddStartTrees = function()
	
		local tmpBound = t.boundsRight;
		local toAdd = 2;
		
		t.boundsRight = t.boundsRight - (toAdd * treeSteps);
		
		for l = 1,toAdd do
			t.AddTree();
			t.boundsRight = t.boundsRight + treeSteps;
		end
		
		t.boundsRight = tmpBound;
	end

------------------------------------------------------------------------

	t.AddTreePart = function(_x,_y,_type)
	
		local tb = love.graphics.newQuad(_x,_y,t.width,t.height,t.image:getWidth(),t.image:getHeight());
		
		if _type == 1 then
			t.tops[#t.tops + 1] = tb;
		elseif _type == 2 then
			t.mids[#t.mids + 1] = tb;
		else
			t.bottoms[#t.bottoms + 1] = tb;
		end
	end
	
------------------------------------------------------------------------

	t.AddTree = function(_size)
	
		_size = _size or t.level;
		local tree = {};
		
		tree.mid = {};
		tree.energy = _size * 10;
		tree.size = _size;
		tree.top    = math.random(#t.tops);
		
		if _size > 1 then
		
			tree.bottom = math.random(#t.bottoms);
		
			_size = _size - 2;
			
			if _size > 0 then	
				for i=1,_size do
					tree.mid[i] = math.random(#t.mids);
				end
			end
		end
		
		tree.x = t.boundsRight;	
		t.trees[#t.trees + 1] = tree;
		
	end
	
------------------------------------------------------------------------

	t.NearestTree = function()
		if #t.trees == 0 then
			return -1;
		end
		
		return t.trees[1].x;
	end
	
------------------------------------------------------------------------

	t.TreeHit = function(_amount)
		if #t.trees == 0 then
			return 0;
		end
		
		t.trees[1].energy = t.trees[1].energy - _amount;
		
		if t.trees[1].energy < 0 then
			t.killed = t.killed + 1;
			if t.killed % 5 == 0 then
				t.level = t.level + 1
			end
			return 2;
		end
		
		return 1;
	
	end
	
------------------------------------------------------------------------
	t.CurrentTreeSize = function()
		return t.trees[1].size;
	end
	
------------------------------------------------------------------------
	
	t.KillTree = function()
		table.remove(t.trees,1);
	end
	
------------------------------------------------------------------------

	t.CountTrees = function()
		return #t.trees;
	end
------------------------------------------------------------------------

	t.Update = function(_dt)
		for k,v in pairs(t.trees) do
			v.x = v.x - (_dt * t.speed);
		end
		
		if #t.trees == 0 or t.trees[#t.trees].x < t.nextTreeMarker then
			t.AddTree();
		end
		
	end

------------------------------------------------------------------------

	t.Draw = function()
	
		for k,v in pairs(t.trees) do
			local pos = t.ground - t.height;
			
			if v.size > 1 then		
				--bottom
				love.graphics.draw(t.image,t.bottoms[v.bottom],v.x,pos);
				pos = pos - t.height;
				
				--mids
				for mk,mv in pairs(v.mid) do
					love.graphics.draw(t.image,t.mids[mv],v.x,pos);
					pos = pos - t.height;
				end
			end
			
			--top
			love.graphics.draw(t.image,t.tops[v.top],v.x,pos);

		end
			
	end
	
	return t;
end
