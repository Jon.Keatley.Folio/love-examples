## ChopChop Timber

A little game I created when first learning Lua. Although the game may not be amazingly fun it does contain a lot of the elements needed to make a game.

It includes the following
* Scene management system
* Sprite font system
* Sprite system which includes support for animation, states, and volocity
* Partical style system
* Parallax scrolling images
